package ru.zingpos.zingpos.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.RemoteException;
import android.util.Log;

import androidx.annotation.Nullable;

import java.util.HashMap;
import java.util.Map;

import ru.evotor.framework.core.IntegrationService;
import ru.evotor.framework.core.action.event.receipt.payment.system.PaymentSystemProcessor;
import ru.evotor.framework.core.action.event.receipt.payment.system.event.PaymentSystemEvent;
import ru.evotor.framework.core.action.event.receipt.payment.system.event.PaymentSystemPaybackCancelEvent;
import ru.evotor.framework.core.action.event.receipt.payment.system.event.PaymentSystemPaybackEvent;
import ru.evotor.framework.core.action.event.receipt.payment.system.event.PaymentSystemSellCancelEvent;
import ru.evotor.framework.core.action.event.receipt.payment.system.event.PaymentSystemSellEvent;
import ru.evotor.framework.core.action.processor.ActionProcessor;

import static ru.zingpos.zingpos.activities.PaymentActivity.EXTRA_NAME_OPERATION;
import static ru.zingpos.zingpos.models.Constants.APP_PREFERENCES;
import static ru.zingpos.zingpos.models.Constants.TERMINAL_TOKEN;

public class PaymentService extends IntegrationService {
    public static final String TAG = "PaymentService";
    SharedPreferences sharedPreferences;

    @Nullable
    @Override
    protected Map<String, ActionProcessor> createProcessors() {
        Map<String, ActionProcessor> processorMap = new HashMap<>();

        processorMap.put(
                PaymentSystemEvent.NAME_ACTION,
                new PaymentSystemProcessor() {

                    @Override
                    public void sell(String s, PaymentSystemSellEvent paymentSystemSellEvent, Callback callback) {
                        Log.e(TAG, "sell " + paymentSystemSellEvent);
                        sharedPreferences = getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE);
                        String userToken = sharedPreferences.getString(TERMINAL_TOKEN, "");

                        Intent intent;
                        if (userToken.isEmpty())
                            intent = new Intent(PaymentService.this, LoginActivity.class);
                        else
                            intent = new Intent(PaymentService.this, PaymentActivity.class);

                        intent.putExtra(EXTRA_NAME_OPERATION, "sell");
                        intent.putExtra("amount", paymentSystemSellEvent.getSum().doubleValue());
                        try {
                            callback.startActivity(intent);
                        } catch (RemoteException exc) {
                            exc.printStackTrace();
                        }
                    }

                    @Override
                    public void sellCancel(String s, PaymentSystemSellCancelEvent paymentSystemSellCancelEvent, Callback callback) {
                        Log.e(TAG, "sellCancel " + paymentSystemSellCancelEvent);
                        sharedPreferences = getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE);
                        String userToken = sharedPreferences.getString(TERMINAL_TOKEN, "");

                        Intent intent;
                        if (userToken.isEmpty())
                            intent = new Intent(PaymentService.this, LoginActivity.class);
                        else
                            intent = new Intent(PaymentService.this, PaymentActivity.class);
                        intent.putExtra(EXTRA_NAME_OPERATION, "sellCancel");
                        try {
                            callback.startActivity(intent);
                        } catch (RemoteException exc) {
                            exc.printStackTrace();
                        }
                    }

                    @Override
                    public void payback(String s, PaymentSystemPaybackEvent paymentSystemPaybackEvent, Callback callback) {
                        Log.e(TAG, "payback " + paymentSystemPaybackEvent);
                        sharedPreferences = getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE);
                        String userToken = sharedPreferences.getString(TERMINAL_TOKEN, "");

                        Intent intent;
                        if (userToken.isEmpty())
                            intent = new Intent(PaymentService.this, LoginActivity.class);
                        else
                            intent = new Intent(PaymentService.this, PaymentActivity.class);
                        intent.putExtra(EXTRA_NAME_OPERATION, "payback");
                        try {
                            callback.startActivity(intent);
                        } catch (RemoteException exc) {
                            exc.printStackTrace();
                        }
                    }

                    @Override
                    public void paybackCancel(String s, PaymentSystemPaybackCancelEvent paymentSystemPaybackCancelEvent, Callback callback) {
                        Log.e(TAG, "paybackCancel " + paymentSystemPaybackCancelEvent);
                        sharedPreferences = getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE);
                        String userToken = sharedPreferences.getString(TERMINAL_TOKEN, "");

                        Intent intent;
                        if (userToken.isEmpty())
                            intent = new Intent(PaymentService.this, LoginActivity.class);
                        else
                            intent = new Intent(PaymentService.this, PaymentActivity.class);
                        intent.putExtra(EXTRA_NAME_OPERATION, "paybackCancel");
                        try {
                            callback.startActivity(intent);
                        } catch (RemoteException exc) {
                            exc.printStackTrace();
                        }
                    }
                });

        return processorMap;
    }
}