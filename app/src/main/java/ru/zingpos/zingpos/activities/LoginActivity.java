package ru.zingpos.zingpos.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.evotor.framework.core.IntegrationActivity;
import ru.zingpos.zingpos.R;
import ru.zingpos.zingpos.models.LoginRequest;
import ru.zingpos.zingpos.models.LoginResponse;
import ru.zingpos.zingpos.repositories.RestTemplate;

import static ru.zingpos.zingpos.activities.PaymentActivity.EXTRA_NAME_OPERATION;
import static ru.zingpos.zingpos.models.Constants.APP_PREFERENCES;
import static ru.zingpos.zingpos.models.Constants.TERMINAL_TOKEN;

public class LoginActivity extends IntegrationActivity {
    private final RestTemplate restTemplate = new RestTemplate();
    EditText etUsername;
    EditText etPassword;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        checkUserAuthorized();

        etUsername = findViewById(R.id.login);
        etPassword = findViewById(R.id.password);
        findViewById(R.id.button_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etUsername.getText().toString().isEmpty()) {
                    etUsername.requestFocus();
                    etUsername.setError("Введите логин!");

                    return;
                }

                if (etPassword.getText().toString().isEmpty()) {
                    etPassword.requestFocus();
                    etPassword.setError("Введите пароль!");

                    return;
                }

                restTemplate.auth(LoginRequest.builder()
                        .username(etUsername.getText().toString())
                        .password(etPassword.getText().toString())
                        .build())
                        .enqueue(new Callback<LoginResponse>() {
                            @Override
                            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                                if (response.body() != null) {
                                    SharedPreferences mSettings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor1 = mSettings.edit();
                                    editor1.putString(TERMINAL_TOKEN, response.body().getToken());
                                    editor1.apply();

                                    Intent intent = new Intent(LoginActivity.this, PaymentActivity.class);
                                    intent.putExtra(EXTRA_NAME_OPERATION, getIntent().getStringExtra(EXTRA_NAME_OPERATION));
                                    intent.putExtra("amount", getIntent().getDoubleExtra("amount", 0.0));
                                    startActivity(intent);
                                }
                            }

                            @Override
                            public void onFailure(Call<LoginResponse> call, Throwable t) {
                                t.fillInStackTrace();
                            }
                        });
            }
        });
    }

    private void checkUserAuthorized() {
        SharedPreferences sharedPreferences = getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE);
        if (sharedPreferences.getString(TERMINAL_TOKEN, "").isEmpty())
            return;
        Toast.makeText(this, "Вы уже авторизованы!", Toast.LENGTH_SHORT).show();
        finish();
    }
}
