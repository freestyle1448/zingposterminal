package ru.zingpos.zingpos.activities;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.google.zxing.WriterException;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.evotor.framework.core.IntegrationActivity;
import ru.evotor.framework.core.action.event.receipt.payment.system.result.PaymentSystemPaymentErrorResult;
import ru.evotor.framework.core.action.event.receipt.payment.system.result.PaymentSystemPaymentOkResult;
import ru.evotor.framework.payment.PaymentType;
import ru.zingpos.zingpos.R;
import ru.zingpos.zingpos.repositories.RestTemplate;

import static ru.zingpos.zingpos.models.Constants.APP_PREFERENCES;
import static ru.zingpos.zingpos.models.Constants.TERMINAL_TOKEN;

public class PaymentActivity extends IntegrationActivity {
    public static final String EXTRA_NAME_OPERATION = "EXTRA_NAME_OPERATION";

    private String userToken;
    private final RestTemplate restTemplate = new RestTemplate();
    private boolean isCheckSend = false;
    SharedPreferences sharedPreferences;

    ProgressBar progressBarPayment;

    TextView doneText;
    ImageView iconDone;
    Button doneButton;
    Button buttonDecline;
    ImageView qrView;
    private Timer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        doneText = findViewById(R.id.doneText);
        iconDone = findViewById(R.id.doneView);
        doneButton = findViewById(R.id.doneButton);
        buttonDecline = findViewById(R.id.buttonDecline);
        buttonDecline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDeclineClick();
            }
        });
        qrView = findViewById(R.id.qrView);
        progressBarPayment = findViewById(R.id.progressBarPayment);

        sharedPreferences = getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE);
        userToken = sharedPreferences.getString(TERMINAL_TOKEN, "");

        if (getIntent().getDoubleExtra("amount", 0.0) == 0.0) {
            Toast.makeText(this, "ZingPOS успешно запущен!", Toast.LENGTH_SHORT).show();
            finish();
        }

        if (getIntent().hasExtra(EXTRA_NAME_OPERATION) && getIntent().getStringExtra(EXTRA_NAME_OPERATION) != null)
            switch (getIntent().getStringExtra(EXTRA_NAME_OPERATION)) {
                case "sell":
                    createInitRequest(getIntent().getDoubleExtra("amount", 0.0));
                    break;
                case "sellCancel":
                    if (getIntent().getStringExtra("transactionId") != null) {
                        transactionDecline();
                    }
                    break;
                case "payback":
                case "paybackCancel":
                    break;
            }
    }

    public void onDeclineClick() {
        if (timer != null) {
            timer.cancel();
            timer.purge();
        }

        timer = null;

        transactionDecline();
    }

    private void createInitRequest(Double decimalAmount) {
        if (decimalAmount > 0.0) {
            restTemplate.createTransaction((long) (decimalAmount * 100))
                    .enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                            if (response.body() != null) {
                                final String transactionId = response.body().get("order_id").getAsString();
                                getIntent().putExtra("transactionId", transactionId);
                                if (transactionId != null) {
                                    startChecking(transactionId);
                                    QRGEncoder qrgEncoder1 = new QRGEncoder(response.body().get("qr_payload").getAsString(),
                                            null, QRGContents.Type.TEXT, 950);

                                    Bitmap bitmap1;
                                    try {
                                        bitmap1 = qrgEncoder1.encodeAsBitmap();
                                        progressBarPayment.setVisibility(View.INVISIBLE);
                                        qrView.setImageBitmap(bitmap1);
                                    } catch (WriterException e) {
                                        e.printStackTrace();
                                    }
                                }
                            } else {
                                Toast.makeText(PaymentActivity.this, "Произошла ошибка, повторите запрос позже.\nОшибка - "
                                        + "Не удалось создать транзакцию!", Toast.LENGTH_LONG).show();
                                setIntegrationResult(new PaymentSystemPaymentErrorResult("error after get request"));
                                PaymentActivity.this.finish();
                            }
                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {
                            System.err.println(t);
                            //TODO вставить обработку ошибки с токеном
                            Toast.makeText(PaymentActivity.this, "Необходим повторный вход!", Toast.LENGTH_LONG).show();
                            setIntegrationResult(new PaymentSystemPaymentErrorResult(t.getMessage()));

                           /* SharedPreferences mSettings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor1 = mSettings.edit();
                            editor1.putString(TERMINAL_TOKEN, null);
                            editor1.apply();

                            Intent intent;
                            intent = new Intent(PaymentActivity.this, LoginActivity.class);
                            intent.putExtra(EXTRA_NAME_OPERATION, getIntent().getStringExtra(EXTRA_NAME_OPERATION));
                            startActivity(intent);*/

                            finish();
                        }
                    });
        }
    }

    private void startChecking(final String transactionId) {
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (!isCheckSend) {
                    isCheckSend = true;
                    restTemplate.checkTransaction(transactionId)
                            .enqueue(new Callback<JsonObject>() {
                                @Override
                                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                    isCheckSend = false;
                                    if (response.body() != null)
                                        if (response.body().get("status").getAsInt() == 2) {
                                            showDone();
                                        }
                                }

                                @Override
                                public void onFailure(Call<JsonObject> call, Throwable t) {
                                    isCheckSend = false;
                                    System.err.println(t);
                                }
                            });
                }
            }
        }, 0, 500);
    }

    public void transactionDecline() {
        if (getIntent().getStringExtra("transactionId") != null) {
            if (!getIntent().getStringExtra("transactionId").isEmpty()) {
                restTemplate.declineTransaction(getIntent().getStringExtra("transactionId")).enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        System.err.println(call);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("psbQR", "");
                        editor.apply();
                        setIntegrationResult(new PaymentSystemPaymentErrorResult("Транзакция отменена!"));
                        PaymentActivity.this.finish();
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        setIntegrationResult(new PaymentSystemPaymentErrorResult(t.getMessage()));
                        PaymentActivity.this.finish();
                    }
                });
            } else {
                setIntegrationResult(new PaymentSystemPaymentErrorResult("Транзакция не существует"));
                PaymentActivity.this.finish();
            }
        } else {
            setIntegrationResult(new PaymentSystemPaymentErrorResult("Транзакция не существует"));
            PaymentActivity.this.finish();
        }
    }

    private void showDone() {
        if (timer != null) {
            timer.cancel();
            timer.purge();
        }

        qrView.setVisibility(View.INVISIBLE);
        buttonDecline.setVisibility(View.INVISIBLE);

        StringBuilder rrn = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            rrn.append(random.nextInt(10));
        }
        List<String> slip = new ArrayList<String>();
        slip.add("SLIP START");
        slip.add("RRN:");
        slip.add(rrn.toString());
        slip.add("SLIP END");
        setIntegrationResult(new PaymentSystemPaymentOkResult(rrn.toString(), slip, "123qwe", PaymentType.ELECTRON));
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timer != null) {
            timer.cancel();
            timer.purge();
        }

        timer = null;

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("psbQR", "");
        editor.apply();
    }

    @Override
    public void onBackPressed() {
        if (timer != null) {
            timer.cancel();
            timer.purge();
        }

        timer = null;

        transactionDecline();
    }
}
