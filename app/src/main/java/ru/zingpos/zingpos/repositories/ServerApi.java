package ru.zingpos.zingpos.repositories;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import ru.zingpos.zingpos.models.LoginResponse;

interface ServerApi {
    @FormUrlEncoded
    @POST("api/user/transaction/create")
    Call<JsonObject> createTransaction(@Field(value = "amount") Long amount);

    @GET("api/transaction/{id}/check")
    Call<JsonObject> checkTransaction(@Path("id") String transactionId);


    @POST("api/transaction/{id}/cancel")
    Call<JsonObject> declineTransaction(@Path("id") String transactionId);

    @POST("api/user/auth")
    @FormUrlEncoded
    Call<LoginResponse> auth(@Field(value = "username") String username, @Field(value = "password") String password);
}
