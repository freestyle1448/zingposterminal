package ru.zingpos.zingpos.repositories;

import com.google.gson.JsonObject;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.zingpos.zingpos.models.LoginRequest;
import ru.zingpos.zingpos.models.LoginResponse;

public class RestTemplate {
    private final String BASE_URL = "https://merchant.zingpos.ru/";
    private final OkHttpClient okHttpClient = UnsafeOkHttpClient.getUnsafeOkHttpClient();
    private final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    private final ServerApi serverApi = retrofit.create(ServerApi.class);

    public Call<JsonObject> createTransaction(Long amount) {
        return serverApi.createTransaction(amount);
    }

    public Call<JsonObject> checkTransaction(String transactionId) {
        return serverApi.checkTransaction(transactionId);
    }

    public Call<JsonObject> declineTransaction(String transactionId) {
        return serverApi.declineTransaction(transactionId);
    }

    public Call<LoginResponse> auth(LoginRequest request) {
        return serverApi.auth(request.getUsername(), request.getPassword());
    }
}

