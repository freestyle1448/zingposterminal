package ru.zingpos.zingpos.models;

import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public
class LoginResponse {
    @SerializedName("err")
    private String err;
    @SerializedName("token")
    private String token;
}
